#!/bin/sh
#
#    Copyright (C) 2022  Luis Guzmán <ark@switnet.org>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#

VERSION=1
. ./config

# Rebrand logo
cp $DATA/png/trisquel-logo_016.png usr/share/icons/Yaru-MATE-light/16x16/places/start-here.png
cp $DATA/png/trisquel-logo_022.png usr/share/icons/Yaru-MATE-light/22x22/places/start-here.png
cp $DATA/png/trisquel-logo_024.png usr/share/icons/Yaru-MATE-light/24x24/places/start-here.png
cp $DATA/png/trisquel-logo_032.png usr/share/icons/Yaru-MATE-light/16x16@2x/places/start-here.png
cp $DATA/png/trisquel-logo_032.png usr/share/icons/Yaru-MATE-light/32x32/places/start-here.png
cp $DATA/png/trisquel-logo_044.png usr/share/icons/Yaru-MATE-light/22x22@2x/places/start-here.png
cp $DATA/png/trisquel-logo_048.png usr/share/icons/Yaru-MATE-light/24x24@2x/places/start-here.png
cp $DATA/png/trisquel-logo_048.png usr/share/icons/Yaru-MATE-light/48x48/places/start-here.png
cp $DATA/png/trisquel-logo_064.png usr/share/icons/Yaru-MATE-light/32x32@2x/places/start-here.png
cp $DATA/png/trisquel-logo_096.png usr/share/icons/Yaru-MATE-light/48x48@2x/places/start-here.png
cp $DATA/png/trisquel-logo_256.png usr/share/icons/Yaru-MATE-light/256x256/places/start-here.png
cp $DATA/png/trisquel-logo_512.png usr/share/icons/Yaru-MATE-light/256x256@2x/places/start-here.png

cp $DATA/png/user-desktop_016.png usr/share/icons/Yaru-MATE-light/16x16/places/user-desktop.png
cp $DATA/png/user-desktop_024.png usr/share/icons/Yaru-MATE-light/24x24/places/user-desktop.png
cp $DATA/png/user-desktop_032.png usr/share/icons/Yaru-MATE-light/16x16@2x/places/user-desktop.png
cp $DATA/png/user-desktop_032.png usr/share/icons/Yaru-MATE-light/32x32/places/user-desktop.png
cp $DATA/png/user-desktop_048.png usr/share/icons/Yaru-MATE-light/24x24@2x/places/user-desktop.png
cp $DATA/png/user-desktop_048.png usr/share/icons/Yaru-MATE-light/48x48/places/user-desktop.png
cp $DATA/png/user-desktop_064.png usr/share/icons/Yaru-MATE-light/32x32@2x/places/user-desktop.png
cp $DATA/png/user-desktop_096.png usr/share/icons/Yaru-MATE-light/48x48@2x/places/user-desktop.png
cp $DATA/png/user-desktop_256.png usr/share/icons/Yaru-MATE-light/256x256/places/user-desktop.png
cp $DATA/png/user-desktop_512.png usr/share/icons/Yaru-MATE-light/256x256@2x/places/user-desktop.png

for i in $(find . -name start-here-symbolic.svg)
do
    cp $DATA/logo-trisquel-dark.svg $i
done

changelog "Rebrand logo for trisquel mate themes."
package
