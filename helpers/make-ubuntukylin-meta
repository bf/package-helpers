#!/bin/sh
#
#    Copyright (C) 2022  Luis Guzmán <ark@switnet.org>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#

VERSION=5
. ./config

rm_pkg_line(){
#find and remove everywhere found.
grep -rl $1 | xargs -r sed -i "/$1/d"
}
# Remove deprecated packages
grep -l ubuntu-drivers-common desktop* | xargs -r sed -i '/ubuntu-drivers-common/d'
grep -l snapd desktop* | xargs -r sed -i '/snapd/d'
grep -l kylin-docs desktop* | xargs -r sed -i '/ubuntu-kylin-docs/d'
grep -l whoopsie desktop* | xargs -r sed -i '/whoopsie/d'
grep -l apport desktop* | xargs -r sed -i '/apport/d'
rm_pkg_line xul-ext-ubufox
rm_pkg_line branding-ubuntu
rm_pkg_line ubuntu-advantage-tools
rm_pkg_line ubuntu-artwork
rm_pkg_line ubuntu-standard
rm_pkg_line ubuntu-minimal
rm_pkg_line ubuntu-settings

#Rename package and fix map.
sed -i "/^Package:/s|ubuntukylin-desktop$|kylin-desktop|" debian/control
sed -i "s|ubuntukylin-|kylin-|g" metapackage-map
sed -i "/^Recommends:/a Provides: ubuntukylin-desktop\nConflicts: ubuntukylin-desktop (<= 0.46+11.0trisquel1)" debian/control

# Rebrand and tweak packages
sed -i 's|Ubuntu|Trisquel|g' debian/control
sed -i 's|ubuntu|trisquel|' README
replace 'firefox' 'abrowser'
replace 'thunderbird' 'icedove'
replace 'fonts-ubuntu' 'fonts-trisquel'
replace 'ubuntu-release-upgrader-gtk' 'trisquel-release-upgrader-gtk'
grep -rl plymouth-theme | xargs -r sed -i '/plymouth-theme/s|ubuntu|trisquel|g'
grep -rl sounds | xargs -r sed -i '/sounds/s|ubuntu|trisquel|g'
grep -rl ubuntu-keyring | xargs -r sed -i 's|ubuntu-keyring|trisquel-keyring|g'
grep -l ubuntu-kylin-software-center desktop* | \
    xargs -r sed -i "s|ubuntu-kylin-software-center|trisquel-app-install|g"
# Keep an eye on later versions as wayland might replace X.org dependencies.
grep -l brltty desktop* | \
    xargs -r sed -i "s|brltty|brltty\nbrltty-x11|g"

# Remove raspimeta package components
rm update.cfg

changelog "Rebuild to drop non-available deprecated and non-free source packages."

package

